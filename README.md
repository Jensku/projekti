# Ravintolaprojekti


// Luo uusi ravintola
 router.post("/", ravintolat.create);
    
    http://localhost:8080/api/ravintolat:
    {
        "id": 5,
        "nimi": "Mäkkäri",
        "osoite": "Mäkkärintie 5",
        "postinumero": "08997",
        "kaupunki": "Vantaa",
        "puhelinnumero": "4438955530",
        "nettisivut": "www.mcd.fi",
        "tyyli": "Pikaruoka",
        "hintataso": "Edullinen",
        "kotiinkuljetus": "Ei",
        "updatedAt": "2020-05-07T12:57:34.641Z",
        "createdAt": "2020-05-07T12:57:34.641Z"
    }
    

    // Hae kaikki ravintolat
    router.get("/", ravintolat.findAll);
    
    http://localhost:8080/api/ravintolat:
    {
            "id": 1,
            "nimi": "Hesburger",
            "osoite": "Hesburgerintie 5",
            "postinumero": "08997",
            "kaupunki": "Espoo",
            "puhelinnumero": "443890",
            "nettisivut": "www.hesburger.fi",
            "tyyli": "Pikaruoka",
            "hintataso": "Edullinen",
            "kotiinkuljetus": "Ei",
            "createdAt": "2020-05-07T10:01:43.000Z",
            "updatedAt": "2020-05-07T11:03:10.000Z"
        },
        ....

    // Hae yksittäinen ravintola
    router.get("/:id", ravintolat.findOne);
    
    http://localhost:8080/api/ravintolat/3:
    {
        "id": 3,
        "nimi": "PizzaHut",
        "osoite": "Pizzakatu 5",
        "postinumero": "00897",
        "kaupunki": "Helsinki",
        "puhelinnumero": "083736892",
        "nettisivut": "www.pizza.fi",
        "tyyli": "Pizzeria",
        "hintataso": "Kallis",
        "kotiinkuljetus": "Kyllä",
        "createdAt": "2020-05-07T12:45:15.000Z",
        "updatedAt": "2020-05-07T12:45:15.000Z"
    }

    // Päivitä yksittäinen ravintola
    router.put("/:id", ravintolat.update);
    
    http://localhost:8080/api/ravintolat/3:
    {
        "message": "Ravintola päivitetty onnistuneesti."
    }

    // Poista yksittäinen ravintola
    router.delete("/:id", ravintolat.delete);
    
    http://localhost:8080/api/ravintolat/3:
    {
        "message": "Ravintolan poisto onnistui!"
    }

    // Poista kaikki ravintolat
    router.delete("/", ravintolat.deleteAll);






