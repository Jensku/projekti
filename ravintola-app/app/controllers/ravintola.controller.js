const db = require("../models");
const Ravintola = db.ravintolat;
const Op = db.Sequelize.Op;

// Ravintolan luonti ja talletus
exports.create = (req, res) => {
    // Validoidaan pyyntö
    if (!req.body.nimi) {
        res.status(400).send({
            message: "Sisältö ei voi olla tyhjä!"
        });
        return;
    }

    // Luodaan ravintola
    const ravintola = {
        nimi: req.body.nimi,
        osoite: req.body.osoite,
        postinumero: req.body.postinumero,
        kaupunki: req.body.kaupunki,
        puhelinnumero: req.body.puhelinnumero,
        nettisivut: req.body.nettisivut,
        tyyli: req.body.tyyli,
        hintataso: req.body.hintataso,
        kotiinkuljetus: req.body.kotiinkuljetus
    };

    // Talletetaan ravintola tietokantaan
    Ravintola.create(ravintola)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Jokin meni pieleen."
            });
        });
};

// Haetaan kaikki ravintolat tietokannasta
exports.findAll = (req, res) => {
    const nimi = req.query.nimi;
    var condition = nimi ? { nimi: { [Op.like]: `%${nimi}%` } } : null;

    Ravintola.findAll({ where: condition })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Jokin meni pieleen"
            });
        });
};

// Haetaan yksittäinen ravintola id:n avulla
exports.findOne = (req, res) => {
    const id = req.params.id;

    Ravintola.findByPk(id)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Virhe haettaessa ravintolaa id:llä " + id
            });
        });
};

// Päivitetään yksittäistä ravintolaa
exports.update = (req, res) => {
    const id = req.params.id;

    Ravintola.update(req.body, {
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Ravintola päivitetty onnistuneesti."
                });
            } else {
                res.send({
                    message: `Ravintolan päivitys ei onnistu id:llä ${id}.`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Virhe päivitettäessä ravintolaa. ID=" + id
            });
        });
};

// Poistetaan kyseisen id:n omaava ravintola
exports.delete = (req, res) => {
    const id = req.params.id;

    Ravintola.destroy({
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Ravintolan poisto onnistui!"
                });
            } else {
                res.send({
                    message: `Ei voida poistaa ravintolaa id:llä ${id}.`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Ravintolan poisto epäonnistui."
            });
        });
};

// Poistetaan kaikki ravintolat tietokannasta
exports.deleteAll = (req, res) => {
    Ravintola.destroy({
        where: {},
        truncate: false
    })
        .then(nums => {
            res.send({ message: `${nums} Ravintolat poistettu onnistuneesti!` });
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Jokin meni pieleen ravintoloita poistettaessa."
            });
        });
};