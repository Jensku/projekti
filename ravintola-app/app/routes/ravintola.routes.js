module.exports = app => {
    const ravintolat = require("../controllers/ravintola.controller.js");

    var router = require("express").Router();

    // Luo uusi ravintola
    router.post("/", ravintolat.create);

    // Hae kaikki ravintolat
    router.get("/", ravintolat.findAll);

    // Hae yksittäinen ravintola
    router.get("/:id", ravintolat.findOne);

    // Päivitä yksittäinen ravintola
    router.put("/:id", ravintolat.update);

    // Poista yksittäinen ravintola
    router.delete("/:id", ravintolat.delete);

    // Poista kaikki ravintolat
    router.delete("/", ravintolat.deleteAll);

    app.use('/api/ravintolat', router);
};