module.exports = (sequelize, Sequelize) => {
    const Ravintola = sequelize.define("ravintolakokeilu", {

        nimi: {
            type: Sequelize.STRING,
            allowNull: false
        },
        osoite: {
            type: Sequelize.STRING,
            allowNull: false
        },
        postinumero: {
            type: Sequelize.STRING,
            allowNull: false
        },
        kaupunki: {
            type: Sequelize.STRING,
            allowNull: false
        },
        puhelinnumero: {
            type: Sequelize.STRING,
            allowNull: true
        },
        nettisivut: {
            type: Sequelize.STRING,
            allowNull: true
        },
        tyyli: {
            type: Sequelize.STRING,
            allowNull: false
        },
        hintataso: {
            type: Sequelize.STRING,
            allowNull: false
        },
        kotiinkuljetus: {
            type: Sequelize.STRING,
            allowNull: false
        }
    });

    return Ravintola;
};