import http from "../http-common";

class RavintolaDataServices {
    getAll() {
        return http.get("/ravintolat");
    }

    get(id) {
        return http.get(`/ravintolat/${id}`);
    }

    create(data) {
        try {
            const response = http.post("/ravintolat", data);
            return response;
        }catch(e){
            console.log(e.message() + "virhe");
        }
    }

    update(id, data) {
        return http.put(`/ravintolat/${id}`, data);
    }

    delete(id) {
        return http.delete(`/ravintolat/${id}`);
    }

    deleteAll() {
        return http.delete(`/ravintolat`);
    }

    findByTitle(nimi) {
        return http.get(`/ravintolat?nimi=${nimi}`);
    }
}

export default new RavintolaDataServices();