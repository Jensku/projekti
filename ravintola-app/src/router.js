import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
    mode: "history",
    routes: [
        {
            path: "/ravintolat",
            name: "ravintolat",
            component: () => import("./components/RavintolaHaku")
        },
        {
            path: "/ravintolat/:id",
            name: "ravintola-details",
            component: () => import("./components/Ravintola")
        },
        {
            path: "/add",
            name: "add",
            component: () => import("./components/RavintolaLomake")
        },
        {
            path: "/",
            name: "etusivu",
            component: () => import("./components/Etusivu")
        }
    ]
});