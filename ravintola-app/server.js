const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const app = express();

var corsOptions = {
    origin: "http://localhost:8081"
};

app.use(cors(corsOptions));

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended: true }));

app.get("/", (req, res) => {
    res.json({ message: "Tervetuloa ravintolatietokantaaan." });
});

const db = require("./app/models");
// force: false estää DROP TABLE IF EXISTS -queryn
db.sequelize.sync({ force: false }).then(() => {
});
require("./app/routes/ravintola.routes")(app);

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log(`Serveri kuuntelee portissa ${PORT}.`);
});